//
//  HCTextProcessor.h
//  HC
//
//  Created by Jason Dinh on 5/4/16.
//  Copyright © 2016 Jason Dinh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constant.h"
@interface HCTextProcessor : NSObject

//return a dictionary with @"mentions", @"links" and @"emoticons" key
- (NSDictionary *) process: (NSString *) string;

@end
