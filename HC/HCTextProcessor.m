//
//  HCTextProcessor.m
//  HC
//
//  Created by Jason Dinh on 5/4/16.
//  Copyright © 2016 Jason Dinh. All rights reserved.
//

#import "HCTextProcessor.h"
@implementation HCTextProcessor

- (NSDictionary *) process: (NSString *) string {
    //return early for nil and invalid input
    if (![string isKindOfClass: [NSString class]]) {
        return nil;
    }
    
    static NSRegularExpression *mentionRegex, *emoticonRegex, *titleRegex;
    static NSDataDetector *linkDetector;
    
    if (!mentionRegex) {
        NSError *error = nil;
        mentionRegex = [NSRegularExpression regularExpressionWithPattern:@"@(\\w+)" options: kNilOptions error: &error];
        if (error) {
            NSLog(@"error trying to init mention regex %@", error);
        }
    }
    
    if (!emoticonRegex) {
        NSError *error = nil;
        emoticonRegex = [NSRegularExpression regularExpressionWithPattern: @"\\(([a-z0-9]{1,15})\\)" options: kNilOptions error: &error];
        if (error) {
            NSLog(@"error trying to init mention regex %@", error);
        }
    }
    
    if (!linkDetector) {
        NSError *error = nil;
        linkDetector = [NSDataDetector dataDetectorWithTypes: NSTextCheckingTypeLink error: &error];
        
        if (error) {
            NSLog(@"error trying to init link detector %@", error);
        }
    }
    
    if (!titleRegex) {
        NSError *error = nil;
        NSLog(@"%@", @"<title>(.+?)<\\/title>");
        titleRegex = [NSRegularExpression regularExpressionWithPattern: @"<title>(.+?)<\\/title>" options: kNilOptions error: &error];
        
        if (error) {
            NSLog(@"error trying to init title regex %@", error);
        }
    }
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    NSMutableArray *mentions = [NSMutableArray array];
    NSMutableArray *emoticons = [NSMutableArray array];
    NSMutableArray *links = [NSMutableArray array];
    
    NSArray *mentionMatches = [mentionRegex matchesInString: string
                                                    options: kNilOptions
                                                      range: NSMakeRange(0, string.length)];
    
    for (NSTextCheckingResult *match in mentionMatches) {
        NSRange wordRange = [match rangeAtIndex: 1];
        NSString *word = [string substringWithRange: wordRange];
        [mentions addObject: word];
    }
    
    NSArray *emoticonMatches = [emoticonRegex matchesInString: string
                                                      options: kNilOptions
                                                        range: NSMakeRange(0, string.length)];
    
    for (NSTextCheckingResult *match in emoticonMatches) {
        NSRange wordRange = [match rangeAtIndex: 1];
        NSString *word = [string substringWithRange: wordRange];
        [emoticons addObject: word];
    }
    
    NSArray *linkMatches = [linkDetector matchesInString: string
                                                 options: kNilOptions
                                                   range: NSMakeRange(0, [string length])];
    
    for (NSTextCheckingResult *match in linkMatches) {
        NSURL *url = [match URL];
        NSString *urlString = [url absoluteString];
        
        NSString *content = [self contentOfURL: url];
        if (content) {
            NSArray *titleMatches = [titleRegex matchesInString: content
                                                        options: kNilOptions
                                                          range: NSMakeRange(0, content.length)];
            BOOL hasTitle = NO;
            for (NSTextCheckingResult *titleMatch in titleMatches) {
                NSRange titleRange = [titleMatch rangeAtIndex: 1];
                NSString *title = [content substringWithRange: titleRange];
                [links addObject: @{LinkURLKey: urlString, LinkTitleKey: title}];
                hasTitle = YES;
                break;
            }
            if (!hasTitle) {
                [links addObject: @{LinkURLKey: urlString, LinkTitleKey: [NSNull null]}];
            }
        }
        
        else {
            [links addObject: @{LinkURLKey: urlString, LinkTitleKey: [NSNull null]}];
        }
    }
    
    if (mentions.count > 0) {
        [dictionary setObject: mentions forKey: MentionsKey];
    }
    
    if (emoticons.count > 0) {
        [dictionary setObject: emoticons forKey: EmoticonsKey];
    }
    
    if (links.count > 0) {
        [dictionary setObject: links forKey: LinksKey];
    }
    
    return dictionary;
}

//return content of a url, synchronous
- (NSString *) contentOfURL: (NSURL *) url {
    NSData *data = [NSData dataWithContentsOfURL: url];
    if (data) {
        return [[NSString alloc] initWithData: [NSData dataWithContentsOfURL: url] encoding: NSUTF8StringEncoding];
    }
    return nil;
}

@end
