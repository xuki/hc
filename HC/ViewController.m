//
//  ViewController.m
//  HC
//
//  Created by Jason Dinh on 5/4/16.
//  Copyright © 2016 Jason Dinh. All rights reserved.
//

#import "ViewController.h"
#import "HCTextProcessor.h"
@interface ViewController ()

@property (strong) HCTextProcessor *textProcessor;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.textProcessor = [[HCTextProcessor alloc] init];
    NSString *string = @"Olympics are starting soon http://www.nbcolympics.com https://facebook.com @chris @bob (smile) (should not trigger)";
    
    //dispatch to background queue because [NSData dataWithContentsOfURL] will block the queue
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSDictionary *dict = [self.textProcessor process: string];
        NSLog(@"Result %@", dict);
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
