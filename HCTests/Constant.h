//
//  Constant.h
//  HC
//
//  Created by Jason Dinh on 5/4/16.
//  Copyright © 2016 Jason Dinh. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const MentionsKey;
extern NSString *const EmoticonsKey;
extern NSString *const LinksKey;
extern NSString *const LinkTitleKey;
extern NSString *const LinkURLKey;

@interface Constant : NSObject

@end
