//
//  Constant.m
//  HC
//
//  Created by Jason Dinh on 5/4/16.
//  Copyright © 2016 Jason Dinh. All rights reserved.
//

#import "Constant.h"

NSString *const MentionsKey = @"mention";
NSString *const EmoticonsKey = @"emoticons";
NSString *const LinksKey = @"links";
NSString *const LinkTitleKey = @"title";
NSString *const LinkURLKey = @"url";

@implementation Constant

@end
