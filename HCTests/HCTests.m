//
//  HCTests.m
//  HCTests
//
//  Created by Jason Dinh on 5/4/16.
//  Copyright © 2016 Jason Dinh. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HCTextProcessor.h"
@interface HCTests : XCTestCase

@property (strong, nonatomic) HCTextProcessor *textProcessor;

@end

@implementation HCTests

- (void)setUp {
    [super setUp];
    self.textProcessor = [[HCTextProcessor alloc] init];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void) testTextProcessor {
    NSString *string1 = @"@chris you around?";
    NSString *string2 = @"Good morning! (megusta) (coffee)";
    NSString *string3 = @"Olympics are starting soon;http://www.nbcolympics.com";
    NSString *string4 = @"@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
    NSString *string5 = @"Olympics are starting soon http://www.nbcolympics.com https://facebook.com @chris @bob (smile) (should not trigger)";
    NSString *string6 = @"http://0.0.0.0 (bogus) (site) @jason";
    
    NSDictionary *dict;
    NSArray *mentions, *emoticons, *links;
    NSDictionary *linkDict;
    NSString *url, *title;
    
    //string1
    dict = [self.textProcessor process: string1];
    mentions = dict[MentionsKey];

    XCTAssertTrue([mentions isKindOfClass:[NSArray class]]);
    XCTAssertTrue(mentions.count == 1);
    XCTAssertTrue([mentions containsObject: @"chris"]);
    
    //string2
    dict = [self.textProcessor process: string2];
    emoticons = dict[EmoticonsKey];
    
    XCTAssertTrue([emoticons isKindOfClass:[NSArray class]]);
    XCTAssertTrue(emoticons.count == 2);
    XCTAssertTrue([emoticons containsObject: @"megusta"]);
    XCTAssertTrue([emoticons containsObject: @"coffee"]);
    
    //string3
    dict = [self.textProcessor process: string3];
    links = dict[LinksKey];
    
    XCTAssertTrue([links isKindOfClass:[NSArray class]]);
    XCTAssertTrue(links.count == 1);
    
    linkDict = [links firstObject];
    XCTAssertTrue([linkDict isKindOfClass: [NSDictionary class]]);
    
    url = linkDict[LinkURLKey];
    XCTAssertTrue([url isKindOfClass:[NSString class]]);
    XCTAssertTrue([url isEqualToString: @"http://www.nbcolympics.com"]);
    
    title = linkDict[LinkTitleKey];
    XCTAssertTrue([title isKindOfClass:[NSString class]]);
    XCTAssertTrue([title isEqualToString: @"2016 Rio Olympic Games | NBC Olympics"]);
    
    //string4
    dict = [self.textProcessor process: string4];
    mentions = dict[MentionsKey];
    emoticons = dict[EmoticonsKey];
    links = dict[LinksKey];
    
    XCTAssertTrue([mentions isKindOfClass:[NSArray class]]);
    XCTAssertTrue(mentions.count == 2);
    XCTAssertTrue([mentions containsObject: @"bob"]);
    XCTAssertTrue([mentions containsObject: @"john"]);
    
    XCTAssertTrue([emoticons isKindOfClass:[NSArray class]]);
    XCTAssertTrue(emoticons.count == 1);
    XCTAssertTrue([emoticons containsObject: @"success"]);
    
    XCTAssertTrue([links isKindOfClass:[NSArray class]]);
    XCTAssertTrue(links.count == 1);
    
    linkDict = [links firstObject];
    XCTAssertTrue([linkDict isKindOfClass: [NSDictionary class]]);
    
    url = linkDict[LinkURLKey];
    XCTAssertTrue([url isKindOfClass:[NSString class]]);
    XCTAssertTrue([url isEqualToString: @"https://twitter.com/jdorfman/status/430511497475670016"]);
    
    title = linkDict[LinkTitleKey];
    XCTAssertTrue([title isKindOfClass:[NSString class]]);
    XCTAssertTrue([title isEqualToString: @"Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"]);
    
    //string5
    dict = [self.textProcessor process: string5];
    mentions = dict[MentionsKey];
    emoticons = dict[EmoticonsKey];
    links = dict[LinksKey];
    
    XCTAssertTrue([mentions isKindOfClass:[NSArray class]]);
    XCTAssertTrue(mentions.count == 2);
    XCTAssertTrue([mentions containsObject: @"chris"]);
    XCTAssertTrue([mentions containsObject: @"bob"]);
    
    XCTAssertTrue([emoticons isKindOfClass:[NSArray class]]);
    XCTAssertTrue(emoticons.count == 1);
    XCTAssertTrue([emoticons containsObject: @"smile"]);
    
    XCTAssertTrue([links isKindOfClass:[NSArray class]]);
    XCTAssertTrue(links.count == 2);
    
    linkDict = [links firstObject];
    XCTAssertTrue([linkDict isKindOfClass: [NSDictionary class]]);
    
    url = linkDict[LinkURLKey];
    XCTAssertTrue([url isKindOfClass:[NSString class]]);
    XCTAssertTrue([url isEqualToString: @"http://www.nbcolympics.com"]);
    
    title = linkDict[LinkTitleKey];
    XCTAssertTrue([title isKindOfClass:[NSString class]]);
    XCTAssertTrue([title isEqualToString: @"2016 Rio Olympic Games | NBC Olympics"]);
    
    linkDict = [links lastObject];
    XCTAssertTrue([linkDict isKindOfClass: [NSDictionary class]]);
    
    url = linkDict[LinkURLKey];
    XCTAssertTrue([url isKindOfClass:[NSString class]]);
    XCTAssertTrue([url isEqualToString: @"https://facebook.com"]);
    
    title = linkDict[LinkTitleKey];
    XCTAssertTrue([title isKindOfClass:[NSString class]]);
    XCTAssertTrue([title isEqualToString: @"Welcome to Facebook"]);
    
    //string6
    dict = [self.textProcessor process: string6];
    mentions = dict[MentionsKey];
    emoticons = dict[EmoticonsKey];
    links = dict[LinksKey];
    
    XCTAssertTrue([mentions isKindOfClass:[NSArray class]]);
    XCTAssertTrue(mentions.count == 1);
    XCTAssertTrue([mentions containsObject: @"jason"]);
    
    XCTAssertTrue([emoticons isKindOfClass:[NSArray class]]);
    XCTAssertTrue(emoticons.count == 2);
    XCTAssertTrue([emoticons containsObject: @"bogus"]);
    XCTAssertTrue([emoticons containsObject: @"site"]);
    
    XCTAssertTrue([links isKindOfClass:[NSArray class]]);
    XCTAssertTrue(links.count == 1);
    
    linkDict = [links firstObject];
    XCTAssertTrue([linkDict isKindOfClass: [NSDictionary class]]);
    
    url = linkDict[LinkURLKey];
    XCTAssertTrue([url isKindOfClass:[NSString class]]);
    XCTAssertTrue([url isEqualToString: @"http://0.0.0.0"]);
    
    title = linkDict[LinkTitleKey];
    XCTAssertTrue([title isKindOfClass:[NSNull class]]);
}
@end
