##HCTextProcessor

`HCTextProcessor` is a simple class to parse text for mentions (`@username`), emoticons (`(emoticons)`) and urls (`http://example.com`). 

###Usage

`HCTextProcessor` should be called on a background queue, it'll block the queue until it finishes downloading the content of the urls detected. 

`HCTextProcessor` only works with web page that set the title in `<title></title>` tag. It does not attempt to render the content thus unable to detect the title set using javascript (`document.title = "example"`). It also does not support redirection.

	NSString *string = @"Olympics are starting soon http://www.nbcolympics.com https://facebook.com @chris @bob (smile)";
    HCTextProcessor *textProcessor = [[HCTextProcessor alloc] init];
    NSDictionary *result = [textProcessor process: string];